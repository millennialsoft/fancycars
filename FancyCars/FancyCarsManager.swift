//
//  FancyCarsManager.swift
//  FancyCars
//
//  Created by Akhlaq Rao on 4/6/19.
//  Copyright © 2019 MillennialSoft. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class FancyCarsManager: NSObject, FancyCarsCacheManagerService {
    
    static let sharedInstance = FancyCarsManager()
    let fancyCarsAPIManager = FancyCarsAPIManager()
    let fancyCarsInstanceManager = Configuration().fancyCarsInstanceManager()
    let fancyCarsAPIErrorFormatUtility = FancyCarsAPIErrorFormatUtility()
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    func getCars(_ page:Int, sortBy:String, completionHandler: @escaping  (([AnyObject]?, NSError?) -> Void) ) {
        if Reachability.isConnectedToNetwork() {
            fancyCarsInstanceManager.getCars(page, completionHandler: { [weak self] (cars, error) in
                if nil == cars {
                    completionHandler(nil, error)
                } else {
                    
                    var mutatedCars = [AnyObject]()
                    for car in cars! {
                        if let self = self {
                            self.fancyCarsInstanceManager.getAvailability(Int(car.id!)!, completionHandler: { (availability, error) in
                                if let availability = availability as? [String:String] {
                                    if let availabilityType = availability["available"] {
                                        car.available = availabilityType
                                    }
                                    mutatedCars.append(car)
                                }
                            })
                        }
                    }
                    
                    completionHandler(mutatedCars, nil)
                    
                    if let self = self {
                        self.cacheCars(cars: mutatedCars as? [Car], completionHandler: { (success, error) in
                            if !success {
                                dlog(message: "\(#function) Could not parse availability json data: \(error!.localizedDescription)!", error: error!, tags: [])
                            }
                        })
                    }
                }
            })
        } else {
            fetchCars(sortBy) { (cars, error) in
                if nil == cars {
                    completionHandler(nil, error)
                } else {
                    completionHandler(cars, nil)
                }
            }
        }
    }
}
