//
//  RequestEngine.swift
//  FancyCars
//
//  Created by Akhlaq Rao on 4/5/19.
//  Copyright © 2019 MillennialSoft. All rights reserved.
//

import Foundation

class RequestEngine: NSObject, URLSessionDelegate, URLSessionTaskDelegate, URLSessionDataDelegate {
    
    typealias RequestCompletionBlock =  (_ data: Data?,  _ statusCode: HttpStatusCode?, _ error: NSError?) -> Void
    
    var requestCompletionBlock: RequestCompletionBlock = { data, code, error in }
    var recievedData: NSMutableData = NSMutableData()
    
    enum RequestType: String {
        
        case Post = "POST"
        case Get = "GET"
        case Patch = "PATCH"
    }
    
    func buildRequest(_ requestType: RequestType, apiURL: String, body: Data?, parameters: [String: Any]?) -> NSMutableURLRequest {
        
        var urlString = apiURL
        print(urlString)
        if nil != parameters {
            
            let parameterString = parameters!.stringFromHttpParameters()
            urlString = "\(apiURL)?\(parameterString)"
        }
        
        let url = URL(string: "\(urlString)")!
        
        let request = NSMutableURLRequest(url: url)
        
        request.httpMethod = requestType.rawValue
        
        request.httpBody = body
        
        request.addValue(headerInformationAttribute.contentType, forHTTPHeaderField: "Content-Type")
        request.addValue(buildBasicAuthenticationHeader(), forHTTPHeaderField: "Authorization")
        request.addValue(headerInformationAttribute.locale(), forHTTPHeaderField: "locale")
        
        return request
    }
    
    func sendRequest(_ request: NSMutableURLRequest, completion: @escaping (_ data: Data?, _ statusCode: HttpStatusCode?, _ error: NSError?) -> Void) {
        requestCompletionBlock = completion
        if Reachability.isConnectedToNetwork() {
            let configuration = URLSessionConfiguration.default
            //configuration.timeoutIntervalForRequest = TimeInterval(5)
            let session = Foundation.URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
            let task = session.dataTask(with: request as URLRequest)
            task.resume()
            session.finishTasksAndInvalidate()
            
        } else {
            
            let noInternetError = NSError(domain: ErrorDomains.FancyCarsAPINetworkingErrorDomain, code: FancyCarsAPINetworkingErrorDomainCode.noInternetConnection.rawValue, userInfo: nil)
            completion(nil, HttpStatusCode.noInternetConnection, noInternetError)
        }
    }
    
    func buildBasicAuthenticationHeader() -> String {
        //To Do: implement Base64 authotization header value.
        return "Authorization Header"
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        //To Do: implement the challenge handling
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if error != nil {
            if task.response != nil {
                requestCompletionBlock(nil, HttpStatusCode(rawValue: (task.response as! HTTPURLResponse).statusCode), error as NSError?)
            }else if (error!._code == NSURLErrorCancelled) {
                requestCompletionBlock(nil, HttpStatusCode.unauthorized, error as NSError?)
            } else {
                requestCompletionBlock(nil, HttpStatusCode.unknown, error as NSError?)
            }
            return
        }
        requestCompletionBlock(recievedData as Data, HttpStatusCode(rawValue: (task.response as! HTTPURLResponse).statusCode), nil)
        
    }
    
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        recievedData.append(data)
    }
    
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void){
        completionHandler(.allow)
    }
}

extension String {
    
    /// Percent escapes values to be added to a URL query as specified in RFC 3986
    ///
    /// This percent-escapes all characters besides the alphanumeric character set and "-", ".", "_", and "~".
    ///
    /// :returns: Returns percent-escaped string.
    
    func stringByAddingPercentEncodingForURLQueryValue() -> String? {
        let allowedCharacters = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~")
        
        return self.addingPercentEncoding(withAllowedCharacters: allowedCharacters)
    }
    
}

extension Dictionary {
    
    /// Build string representation of HTTP parameter dictionary of keys and objects
    ///
    /// This percent escapes in compliance with RFC 3986
    ///
    ///
    /// :returns: String representation in the form of key1=value1&key2=value2 where the keys and values are percent escaped
    
    func stringFromHttpParameters() -> String {
        let parameterArray = self.map { (key, value) -> String in
            let percentEscapedKey = (key as! String).stringByAddingPercentEncodingForURLQueryValue()!
            let percentEscapedValue = (value as! String).stringByAddingPercentEncodingForURLQueryValue()!
            return "\(percentEscapedKey)=\(percentEscapedValue)"
        }
        
        return parameterArray.joined(separator: "&")
    }
    
}
