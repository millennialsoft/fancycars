//
//  FancyCarsTableViewCell.swift
//  FancyCars
//
//  Created by Akhlaq Rao on 4/6/19.
//  Copyright © 2019 MillennialSoft. All rights reserved.
//

import UIKit

class FancyCarsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var buyButton: UIButton!
    
    @IBOutlet weak var thumbImageView: UIImageView!
    
    @IBOutlet weak var detailsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateAppearanceFor(_ image: UIImage?) {
        DispatchQueue.main.async { [weak self] in
            guard let `self` = self else { return }
            self.displayImage(image)
        }
    }
    
    private func displayImage(_ image: UIImage?) {
        if let _image = image {
            thumbImageView.image = _image
        } else {
            thumbImageView.image = .none
        }
    }
}
