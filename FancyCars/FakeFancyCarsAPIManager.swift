//
//  FakeFancyCarsAPIManager.swift
//  FancyCars
//
//  Created by Akhlaq Rao on 4/5/19.
//  Copyright © 2019 MillennialSoft. All rights reserved.
//

import Foundation

open class FakeFancyCarsAPIManager: NSObject, FancyCarsAPIManagerService {
    
    fileprivate func getResponseForKey(_ key: String) -> Data {
        if let path = Bundle.main.path(forResource: "StubResponse", ofType: "plist") {
            let dictionary =  NSDictionary(contentsOfFile: path)!
            if let value = dictionary[key]{
                return (value as AnyObject).data(using: String.Encoding.utf8.rawValue)!
            }
        }
        return "".data(using: String.Encoding.utf8)!
    }
    
    public func getCars(_ page: Int, completionHandler: @escaping (([Car]?, NSError?) -> Void)) {
        let data = getResponseForKey("fancycars.api.cars." + String(page))
        
        let parser = FancyCarsAPIParsingEngine()
        let cars = parser.carsWithJSON(data)
        if nil == cars {
            let formattedError = FancyCarsManager.sharedInstance.fancyCarsAPIErrorFormatUtility.formattedErrorWithFancyCarsAPIStatusCode(.unknown)
            completionHandler(nil, formattedError)
        } else {
            completionHandler(cars, nil)
        }
    }
    
    public func getAvailability(_ id: Int, completionHandler: @escaping (([String : AnyObject]?, NSError?) -> Void)) {
        let data = getResponseForKey("fancycars.api.availability." + String(id))
        
        let parser = FancyCarsAPIParsingEngine()
        let availability = parser.availabilityWithJSON(data)
        if nil == availability {
            let formattedError = FancyCarsManager.sharedInstance.fancyCarsAPIErrorFormatUtility.formattedErrorWithFancyCarsAPIStatusCode(.unknown)
            completionHandler(nil, formattedError)
        } else {
            completionHandler(availability, nil)
        }
    }
}
