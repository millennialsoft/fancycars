//
//  RequestProtocol.swift
//  FancyCars
//
//  Created by Akhlaq Rao on 4/5/19.
//  Copyright © 2019 MillennialSoft. All rights reserved.
//

import Foundation

public enum HttpStatusCode: Int {
    
    case unknown = -1 // When be services is down
    case noInternetConnection = 0
    case ok = 200
    case created = 201
    case badRequest = 400
    case unauthorized = 401
    case forbidden = 403
    case notFound = 404
    case methodNotAllowed = 405
    case proxyAuthenticationRequired = 407
    case internalServerError = 500
    case badGateway = 502
    case gatewayTimeout = 504
    case servicesDown = 10000
    case jointApplicationError = 121212
}

/// RequestProtocol defines how to make network requests
public protocol RequestProtocol {
    /// builds a request object given the type of request
    /// requestType: type of request to make, HTTP GET, HTTP POST, etc
    /// requestURL: the end of the url to build a request for.
    /// apiURL: the ip or DNS address to send the request for
    ///   for example, for a request to http://example.com/exampleURL,
    ///   apiURL is 'http://example.com/', requestURL is 'example'
    /// body: the body of request, for POST body etc
    ///
    /// returns the built request to be executed on demand
    func buildRequest(_ requestType: RequestType, apiURL: String, body: Data) -> NSMutableURLRequest
    
    /// executes a request
    /// request: the request to execute
    /// completion: function that defines what to do when the request is completed
    ///
    func makeRequest(_ request: NSMutableURLRequest, completion:@escaping (_ jsonResponse: [String:AnyObject]?, _ statusCode: HttpStatusCode?, _ errorMessage: String?) -> Void)
}
