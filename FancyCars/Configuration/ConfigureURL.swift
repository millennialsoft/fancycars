//
//  ConfigureURL.swift
//  FancyCars
//
//  Created by Akhlaq Rao on 4/5/19.
//  Copyright © 2019 MillennialSoft. All rights reserved.
//

import Foundation

open class ConfigureURL: NSObject {
    
    open func value(_ forKey: String) -> AnyObject {
        let overrides = config("Override")
        let defaults = config("Configuration")
        if let overridenValue = overrides[forKey] {
            return overridenValue as AnyObject
        }
        if let defaultValue = defaults[forKey] {
            return defaultValue as AnyObject
        }
        return "" as AnyObject
    }
    open func fancyCarsAPIHost() -> String {
        if let conf =  UserDefaults.standard.dictionary(forKey: "com.apple.configuration.managed") {
            return conf["fancycars.api.host"] as! String
        } else {
            return value("fancycars.api.host") as! String
        }
    }
    
    fileprivate func config(_ name: String) -> NSDictionary {
        if let path = Bundle.main.path(forResource: name, ofType: "plist") {
            return NSDictionary(contentsOfFile: path)!
        }
        return NSDictionary()
    }
    fileprivate func buildApiURL(_ host: String, uri: String ) -> String {
        return "\(host)\(uri)"
    }
}
