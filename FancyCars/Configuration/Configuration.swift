//
//  Configuration.swift
//  FancyCars
//
//  Created by Akhlaq Rao on 4/5/19.
//  Copyright © 2019 MillennialSoft. All rights reserved.
//

import Foundation

open class Configuration {
    
    open func fancyCarsInstanceManager() -> FancyCarsAPIManagerService {
        let implementation = value("fancycars.api.manager") as! String
        let classType = NSClassFromString(implementation) as! FancyCarsAPIManagerService.Type
        if classType == type(of: FancyCarsAPIManager.self) {
            return FancyCarsAPIManager()
        } else {
            return FakeFancyCarsAPIManager()
        }
    }
    
    open func value(_ forKey: String) -> AnyObject {
        let overrides = config("Override")
        let defaults = config("Configuration")
        if let overridenValue = overrides[forKey] {
            return overridenValue as AnyObject
        }
        if let defaultValue = defaults[forKey] {
            return defaultValue as AnyObject
        }
        return "" as AnyObject
    }
    
    fileprivate func fancyCarsAPIHost() -> String {
        return value("fancycars.api.host") as! String
    }
    
    fileprivate func config(_ name: String) -> NSDictionary {
        if let path = Bundle.main.path(forResource: name, ofType: "plist") {
            return NSDictionary(contentsOfFile: path)!
        }
        return NSDictionary()
    }
    
    fileprivate func buildApiURL(_ host: String, uri: String ) -> String {
        return "\(host)\(uri)"
    }
}
