//
//  FancyCarsAPIConstants.swift
//  FancyCars
//
//  Created by Akhlaq Rao on 4/5/19.
//  Copyright © 2019 MillennialSoft. All rights reserved.
//

import Foundation

struct ErrorDomains {
    static let FancyCarsAPINetworkingErrorDomain = "api.networking.fancycars"
}

enum FancyCarsAPINetworkingErrorDomainCode: Int {
    case noInternetConnection
}

struct headerInformationAttribute {
    static let contentType = "application/json"
    
    static func locale() -> String {
        let locale = Locale.current
        guard let languageCode = locale.languageCode
            else {
                return "en"
        }
        return languageCode
    }
}

struct ErrorResponseAttribute {
    static let message = "message"
}

// MARK: - REST Routes

struct FancyCarsAPIRESTRoute {
    
    static let routeBase = ConfigureURL().fancyCarsAPIHost()
    
    static let routeCars = routeBase + (ConfigureURL().value("fancycars.api.getCars") as! String)
    
    static func routeAvailability(_ carId: Int) -> String {
        let availibility = (ConfigureURL().value("fancycars.api.getAvailability") as! String)
        let availibilityURL = String(format: availibility, carId)
        return routeBase + availibilityURL
    }
}

// MARK: - Parameters

enum FancyCarsAPICarIdParamterKey: String {
    case id = "id"
}

// MARK: - JSON Attributes (API Contract)

struct FancyCarsAPIRootAttribute {
    static let cars = "cars"
}

struct CarAttribute {
    static let id = "id"
    static let image_url = "img"
    static let year = "year"
    static let name = "name"
    static let make = "make"
    static let model = "model"
}

struct CarAvailabilityAttribute {
    static let available = "available"
}
