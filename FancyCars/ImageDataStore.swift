//
//  ImageDataStore.swift
//  FancyCars
//
//  Created by Akhlaq Rao on 4/8/19.
//  Copyright © 2019 MillennialSoft. All rights reserved.
//

import Foundation
import UIKit.UIImage

class ImageDataStore {
    var images = [ImageModel]()
    
    public var numberOfImage: Int {
        return images.count
    }
    
    public func loadImage(at index: Int) -> DataLoadOperation? {
        if (0..<images.count).contains(index) {
            return DataLoadOperation(images[index])
        }
        return .none
    }
}

class DataLoadOperation: Operation, FancyCarsCacheManagerService {
    var image: UIImage?
    var loadingCompleteHandler: ((UIImage?) -> ())?
    private var _image: ImageModel
    
    init(_ image: ImageModel) {
        _image = image
    }
    
    override func main() {
        if isCancelled { return }
        guard let url = _image.url else { return }
        fetchImage(url.description) { (image, error) in
            if let image = image {
                self.image = image
                self.loadingCompleteHandler?(self.image)
            }else {
                downloadImageFrom(url) { (image) in
                    DispatchQueue.main.async() { [weak self] in
                        guard let `self` = self else { return }
                        if self.isCancelled { return }
                        self.image = image
                        self.loadingCompleteHandler?(self.image)
                        
                        self.cacheImage(image: image, url: url.description, completionHandler: { (success, error) in
                            if !success {
                                print("Fatal Error")
                            }
                        })
                    }
                }
            }
        }
    }
}

func downloadImageFrom(_ url: URL, completeHandler: @escaping (UIImage?) -> ()) {
    URLSession.shared.dataTask(with: url) { data, response, error in
        guard
            let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
            let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
            let data = data, error == nil,
            let _image = UIImage(data: data)
            else { return }
        completeHandler(_image)
        }.resume()
}
