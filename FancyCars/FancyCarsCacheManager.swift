//
//  FancyCarsCacheManager.swift
//  FancyCars
//
//  Created by Akhlaq Rao on 4/5/19.
//  Copyright © 2019 MillennialSoft. All rights reserved.
//

import UIKit
import CoreData

public protocol FancyCarsCacheManagerService {
    
    func fetchCars(_ predicate: String, completionHandler: @escaping  ([NSManagedObject]?, NSError?)->())
    func cacheCars(cars:[Car]?, completionHandler: @escaping  (Bool, NSError?)->())
    func cacheImage(image:UIImage?, url:String?, completionHandler: @escaping  (Bool, NSError?)->())
    func fetchImage(_ predicate: String, completionHandler: @escaping  (UIImage?, NSError?)->())
}

extension FancyCarsCacheManagerService {
    
    func cacheCars(cars: [Car]?, completionHandler: @escaping (Bool, NSError?) -> ()) {
        
        //1
        guard let appDelegate =
            FancyCarsManager.sharedInstance.appDelegate else {
                return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        managedContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "CarEntity",
                                                in: managedContext)!
        
        guard let cars = cars else {
            return
        }
        
        for car in cars {
            let managedCar = NSManagedObject(entity: entity,
                                             insertInto: managedContext)
            // 3
            managedCar.setValue(car.id, forKeyPath: "id")
            managedCar.setValue(car.name, forKeyPath: "name")
            managedCar.setValue(car.make, forKeyPath: "make")
            managedCar.setValue(car.model, forKeyPath: "model")
            managedCar.setValue(car.year, forKeyPath: "year")
            managedCar.setValue(car.image_url, forKeyPath: "image_url")
            managedCar.setValue(car.available, forKeyPath: "available")
        }
        
        do {
            try managedContext.save()
            completionHandler(true, nil)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
            completionHandler(false, error)
        }
    }
    
    func cacheImage(image:UIImage?, url:String?, completionHandler: @escaping  (Bool, NSError?)->()) {
        
        //1
        guard let appDelegate =
            FancyCarsManager.sharedInstance.appDelegate else {
                return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        managedContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "ImageEntity",
                                                in: managedContext)!
        
        guard let url = url else { return }
        guard let image = image else { return }
        
        let managedImage = NSManagedObject(entity: entity,
                                           insertInto: managedContext)
        // 3
        managedImage.setValue(url, forKeyPath: "url")
        let data = image.jpegData(compressionQuality: 1.0)
        managedImage.setValue(data, forKeyPath: "data")
        
        do {
            try managedContext.save()
            completionHandler(true, nil)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
            completionHandler(false, error)
        }
    }
    
    func fetchImage(_ predicate: String, completionHandler: @escaping  (UIImage?, NSError?)->()) {
        
        var managedImages: [NSManagedObject] = []
        
        //1
        guard let appDelegate =
            FancyCarsManager.sharedInstance.appDelegate else {
                return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "ImageEntity")
        
        fetchRequest.predicate = NSPredicate(format: "url = %@", predicate)
        fetchRequest.returnsObjectsAsFaults = false
        
        //3
        do {
            managedImages = try managedContext.fetch(fetchRequest)
            if let managedImage = managedImages.first {
                let image = UIImage(data: managedImage.value(forKey: "data") as! Data)
                completionHandler(image, nil)
            } else {
                completionHandler(nil, nil)
            }
        } catch let error as NSError {
            completionHandler(nil, error)
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func fetchCars(_ predicate: String, completionHandler: @escaping  ([NSManagedObject]?, NSError?)->()) {
        
        var cars: [NSManagedObject] = []
        
        //1
        guard let appDelegate =
            FancyCarsManager.sharedInstance.appDelegate else {
                return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "CarEntity")
        
        let sort = NSSortDescriptor(key: predicate, ascending: true)
        fetchRequest.sortDescriptors = [sort]
        
        //3
        do {
            cars = try managedContext.fetch(fetchRequest)
            completionHandler(cars, nil)
        } catch let error as NSError {
            completionHandler(nil, error)
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
}
