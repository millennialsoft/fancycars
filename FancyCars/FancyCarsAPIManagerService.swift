//
//  FancyCarsAPIManagerService.swift
//  FancyCars
//
//  Created by Akhlaq Rao on 4/5/19.
//  Copyright © 2019 MillennialSoft. All rights reserved.
//

import Foundation

public protocol FancyCarsAPIManagerService {
    
    func getCars(_ page: Int, completionHandler: @escaping (([Car]?, NSError?) -> Void))
    
    func getAvailability(_ id: Int, completionHandler: @escaping (([String : AnyObject]?, NSError?) -> Void))
}
