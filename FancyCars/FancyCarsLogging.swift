//
//  FancyCarsLogging.swift
//  FancyCars
//
//  Created by Akhlaq Rao on 4/5/19.
//  Copyright © 2019 MillennialSoft. All rights reserved.
//

import Foundation

let keyEId = "eid"

#if RELEASE
func elog(eid:NSError, tag: String, message: String, file: String = #file, method: String = #function, line: UInt = #line){
    print(eid, tag, message, file, method, line)
}

func elog(eid:NSError, tags: [String], message: String, file: String = #file, method: String = #function, line: UInt = #line){
    let object = [keyEId:eid.code] as NSDictionary
    print(tags, message, file, method, line, object)
}

func ilog(eid:NSError, tag: String, message: String, file: String = #file, method: String = #function, line: UInt = #line){
    print(eid, tag, message, file, method, line)
}

func wlog(error:NSError, tags: [String] = [], file: String = #file, method: String = #function, line: UInt = #line){
    print(tags, error.localizedDescription, file, method, line)
}

func ilog(eid:NSError, tags: [String], message: String, file: String = #file, method: String = #function, line: UInt = #line){
    let object = [keyEId:eid.code] as NSDictionary
    print(tags, message, file, method, line, object)
}
#else
func dlog(message:String, error:NSError, tags:[String] = [], file:String = #file, method:String = #function, line:UInt = #line){
    print(error.localizedDescription, tags, file, method, line)
}
#endif
