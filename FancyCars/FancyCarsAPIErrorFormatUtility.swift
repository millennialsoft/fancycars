//
//  FancyCarsAPIErrorFormatUtility.swift
//  FancyCars
//
//  Created by Akhlaq Rao on 4/5/19.
//  Copyright © 2019 MillennialSoft. All rights reserved.
//

import UIKit

class FancyCarsAPIErrorFormatUtility: NSObject {
    var statusCode:HttpStatusCode?
    
    func formattedErrorWithFancyCarsAPIStatusCode(_ code: HttpStatusCode?, data: Data? = nil) -> NSError {
        statusCode = code
        var description = NSLocalizedString("Unknown Error", comment: "")
        var failureReason = NSLocalizedString("Unknown Reason", comment: "")
        if let statusCode = statusCode {
            switch statusCode {
            case .notFound:
                description = NSLocalizedString("Not Found Error", comment: "")
                break
            case .unauthorized:
                description = NSLocalizedString("Not Authorized Error", comment: "")
                break
            case .badRequest:
                description = NSLocalizedString("Bad Request Error", comment: "")
                break
            case .noInternetConnection:
                description = NSLocalizedString("Network Error", comment: "")
                break
            case .internalServerError:
                description = NSLocalizedString("System Error", comment: "")
                if let errorData = data, let errorMsg = getErrorMsgFromData(errorData) {
                    failureReason = errorMsg
                }
            default:
                break
            }
        } else{
            statusCode = .unknown
        }
        let errorDict =  [NSLocalizedDescriptionKey:description, NSLocalizedFailureReasonErrorKey:failureReason]
        let error: NSError = NSError(domain: ErrorDomains.FancyCarsAPINetworkingErrorDomain, code: statusCode!.rawValue, userInfo: errorDict)
        return error
    }
    
    func getErrorMsgFromData(_ data: Data) -> String? {
        //try to parse the data
        do {
            
            if let rootJSON = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary {
                return rootJSON[ErrorResponseAttribute.message] as? String
            }
            
        } catch {
            print("\(#function) Could not parse error json data: \(error)")
        }
        
        return nil
    }
}

