//
//  Request.swift
//  FancyCars
//
//  Created by Akhlaq Rao on 4/5/19.
//  Copyright © 2019 MillennialSoft. All rights reserved.
//

import Foundation

public enum RequestType: String {
    case Post = "POST"
    case Get = "GET"
}

open class Request: NSObject, RequestProtocol {
    /// builds a request object given the type of request
    /// requestType: type of request to make, HTTP GET, HTTP POST, etc
    /// requestURL: the end of the url to build a request for.
    /// apiURL: the ip or DNS address to send the request for
    ///   for example, for a request to http://example.com/exampleURL,
    ///   apiURL is 'http://example.com/', requestURL is 'example'
    /// body: the body of request, for POST body etc
    ///
    /// returns the built request to be executed on demand
    open func buildRequest(_ requestType: RequestType, apiURL: String, body: Data) -> NSMutableURLRequest {
        //let url = NSURL(string: "\(apiURL)\(requestURL.rawValue)")!
        let url = URL(string: "\(apiURL)")!
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = requestType.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Basic c2xlNTBpOnBhc3N3b3Jk", forHTTPHeaderField: "Authorization")
        if requestType == .Post {
            request.httpBody = body
        }
        return request
    }
    
    /// executes a request
    /// request: the request to execute
    /// completion: function that defines what to do when the request is completed
    ///
    open func makeRequest(_ request: NSMutableURLRequest, completion: @escaping (_ jsonResponse: [String:AnyObject]?, _ statusCode: HttpStatusCode?, _ errorMessage: String?) -> Void) {
        if Reachability.isConnectedToNetwork() {
            var statusCode = 200
            
            
            let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                if error != nil {
                    completion(nil, nil, error!.localizedDescription) //request failed, return directly
                    return
                }
                
                if response is HTTPURLResponse {
                    statusCode = (response as! HTTPURLResponse).statusCode
                }
                
                //try to parse the data
                do {
                    if let data = data {
                        if let responseJSON = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject] {
                            completion(responseJSON, HttpStatusCode(rawValue: statusCode), "")
                        } else {
                            completion(nil, HttpStatusCode(rawValue: statusCode), "Failed to parse the data")
                        }
                    } else {
                        completion(nil, HttpStatusCode(rawValue: statusCode), (error?.localizedDescription)!)
                    }
                } catch _ {
                    completion(nil, HttpStatusCode(rawValue: statusCode), "Exception while de serializing data")
                }
                
            })
            task.resume()
        }else {
            completion(nil, HttpStatusCode.noInternetConnection, "No Internet Connection")
        }
        
    }
}
