//
//  Car.swift
//  FancyCars
//
//  Created by Akhlaq Rao on 4/5/19.
//  Copyright © 2019 MillennialSoft. All rights reserved.
//

import Foundation

enum FancyCarsAPICarAvailabilityType: String {
    case InDealership = "In Dealership"
    case OutofStock = "Out of Stock"
    case Unavailable = "Unavailable"
}

open class Car: NSObject {
    
    @objc var year: String?
    @objc var id: String?
    @objc var name: String?
    @objc var make: String?
    @objc var model: String?
    @objc var image_url: String?
    @objc var available = FancyCarsAPICarAvailabilityType.Unavailable.rawValue
    
    // MARK: - Representations
    //
    internal func JSONRepresentation() -> Data {
        
        let dictionary = dictionaryRepresentation()
        var data = Data()
        do {
            data = try JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
        } catch let error as NSError {
            print(error)
        }

        return data
    }
    
    internal func dictionaryRepresentation() -> [String: Any] {
        
        var dictionary = [String: Any]()
        
        dictionary[CarAttribute.id] = id as AnyObject?
        dictionary[CarAttribute.year] = year as AnyObject?
        dictionary[CarAttribute.name] = name as AnyObject?
        dictionary[CarAttribute.make] = make as AnyObject?
        dictionary[CarAttribute.model] = model as AnyObject?
        dictionary[CarAttribute.image_url] = image_url as AnyObject?
        
        return [FancyCarsAPIRootAttribute.cars:dictionary as AnyObject]
    }
    
    // MARK: - Object Mapper
    //
    internal static func map(_ dict: [String:Any]) -> Car {
        
        let car = Car()
        
        car.id = dict[CarAttribute.id] as? String
        if let year = dict[CarAttribute.year] as? Int {
            car.year = String(year)
        }
        
        car.name = dict[CarAttribute.name] as? String
        car.make = dict[CarAttribute.make] as? String
        car.model = dict[CarAttribute.model] as? String
        car.image_url = dict[CarAttribute.image_url] as? String

        return car
    }
}
