//
//  FancyCarsAPIManager.swift
//  FancyCars
//
//  Created by Akhlaq Rao on 4/5/19.
//  Copyright © 2019 MillennialSoft. All rights reserved.
//

import Foundation

class FancyCarsAPIManager: NSObject, FancyCarsAPIManagerService {
    
    func getCars(_ page: Int, completionHandler: @escaping (([Car]?, NSError?) -> Void)) {
        let requestEngine = RequestEngine()
        let request = requestEngine.buildRequest(.Get, apiURL: FancyCarsAPIRESTRoute.routeCars, body: nil, parameters: nil)
        
        requestEngine.sendRequest(request, completion: { (data, code, error) in
            
            if code != .ok {
                
            let formattedError = FancyCarsManager.sharedInstance.fancyCarsAPIErrorFormatUtility.formattedErrorWithFancyCarsAPIStatusCode(code!)
                completionHandler(nil, formattedError)
                return
            }
            
            let parser = FancyCarsAPIParsingEngine()
            let car = parser.carsWithJSON(data!)
            
            completionHandler(car, nil)
        })
    }
    
    func getAvailability(_ id: Int, completionHandler: @escaping (([String : AnyObject]?, NSError?) -> Void)) {
        let requestEngine = RequestEngine()
        let request = requestEngine.buildRequest(.Get, apiURL: FancyCarsAPIRESTRoute.routeAvailability(id), body: nil, parameters: [FancyCarsAPICarIdParamterKey.id.rawValue:id])
        
        requestEngine.sendRequest(request, completion: { (data, code, error) in
            
            if code != .ok {
                
                let formattedError = FancyCarsManager.sharedInstance.fancyCarsAPIErrorFormatUtility.formattedErrorWithFancyCarsAPIStatusCode(code!)
                completionHandler(nil, formattedError)
                return
            }
            
            let parser = FancyCarsAPIParsingEngine()
            let availability = parser.availabilityWithJSON(data!)
            
            completionHandler(availability, nil)
        })
    }
}
