//
//  FancyCarsAPIParsingEngine.swift
//  FancyCars
//
//  Created by Akhlaq Rao on 4/5/19.
//  Copyright © 2019 MillennialSoft. All rights reserved.
//

import Foundation

class FancyCarsAPIParsingEngine: NSObject {
    
    // MARK: - Party Parsing Section
    
    func carsWithJSON(_ data: Data) -> [Car]? {
        
        //try to parse the data
        do {
            
            if let rootJSON = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? NSDictionary {
                if let objectJSON = rootJSON[FancyCarsAPIRootAttribute.cars] as? [[String:AnyObject]] {
                    
                    var cars = [Car]()
                    
                    for carDict in objectJSON {
                        let car = Car.map(carDict)
                        cars.append(car)
                    }
                    return cars
                }
            }
            
        } catch {
            dlog(message: "\(#function) Could not parse availability json data: \(error.localizedDescription) \n \(String(describing: String(data: data, encoding: String.Encoding.utf8)))!", error: error as NSError, tags: [])
        }
        
        return nil
    }
    
    func availabilityWithJSON(_ data: Data) -> [String:AnyObject]? {
        
        //try to parse the data
        do {
            
            if let rootJSON = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject] {
                return rootJSON
            }
        } catch {
            dlog(message: "\(#function) Could not parse availability json data: \(error.localizedDescription) \n \(String(describing: String(data: data, encoding: String.Encoding.utf8)))!", error: error as NSError, tags: [])
        }
        
        return nil
    }
    
    
    // MARK: -
    // end of Party Parsing Section
}
