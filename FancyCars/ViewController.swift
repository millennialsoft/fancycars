//
//  ViewController.swift
//  FancyCars
//
//  Created by Akhlaq Rao on 4/5/19.
//  Copyright © 2019 MillennialSoft. All rights reserved.
//

import UIKit
import CoreData
import Foundation

class ViewController: UIViewController {
    
    fileprivate var cars: [AnyObject] = []
    fileprivate let pageSize = 10
    fileprivate var predicate = "name"
    
    private lazy var dataStore = ImageDataStore()
    private lazy var loadingQueue = OperationQueue()
    private lazy var loadingOperations = [IndexPath : DataLoadOperation]()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        title = "Fancy Cars"
        tableView.prefetchDataSource = self
        self.fetchCars(sortBy: self.predicate, page: (cars.count + pageSize)/pageSize)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func sortCars(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "Fancy Cars", message: "Sort By", preferredStyle: .actionSheet)
        
        let sortByAvailabilityAction = UIAlertAction(title: "Availability", style: .default, handler: {(alert :UIAlertAction!) in
            self.predicate = "available"
            FancyCarsManager.sharedInstance.fetchCars(self.predicate, completionHandler: { (cars, error) in
                if let cars = cars {
                    self.cars = cars
                    self.dataStore.images = self.cars.map({ ImageModel(url: $0.image_url) })
                    self.tableView.reloadData()
                }
            })
        })
        alertController.addAction(sortByAvailabilityAction)
        
        let sortByNameAction = UIAlertAction(title: "Name", style: .default, handler: {(alert :UIAlertAction!) in
            self.predicate = "name"
            FancyCarsManager.sharedInstance.fetchCars(self.predicate, completionHandler: { (cars, error) in
                if let cars = cars {
                    self.cars = cars
                    self.dataStore.images = self.cars.map({ ImageModel(url: $0.image_url) })
                    self.tableView.reloadData()
                }
            })
        })
        alertController.addAction(sortByNameAction)
        
        alertController.popoverPresentationController?.sourceView = view
        let barButtonItem = self.navigationItem.rightBarButtonItem!
        let buttonItemView = barButtonItem.value(forKey: "view") as? UIView
        alertController.popoverPresentationController?.sourceRect = buttonItemView!.frame
        
        present(alertController, animated: true, completion: nil)
    }
    
    func isLoadingCell(indexPath: IndexPath) -> Bool {
        return indexPath.row >= cars.count - 3
    }
    
    func loadImages() {
        self.dataStore.images = self.cars.map({ ImageModel(url: $0.image_url) })
    }
    
    func fetchCars(sortBy: String, page: Int) {
        FancyCarsManager.sharedInstance.getCars(page, sortBy: sortBy) { (cars, error) in
            DispatchQueue.main.async { [weak self] in
                if let self = self {
                    if let cars = cars {
                        for car in cars {
                            if Car.self != type(of: car) {
                                self.cars = cars
                                self.loadImages()
                                self.tableView.reloadData()
                                return
                            }
                            self.cars.append(car)
                        }
                        if self.predicate == "available" {
                            self.cars.sort { $0.available < $1.available }
                        } else {
                            self.cars.sort { $0.name < $1.name }
                        }
                        self.loadImages()
                        self.tableView.reloadData()
                    } else {
                        //TO DO: educate user about the fatal error.
                        print(error.debugDescription)
                    }
                }
            }
        }
    }
}

// MARK:- TableView Delegate
extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? FancyCarsTableViewCell else { return }
        
        // How should the operation update the cell once the data has been loaded?
        let updateCellClosure: (UIImage?) -> () = { [unowned self] (image) in
            cell.updateAppearanceFor(image)
            self.loadingOperations.removeValue(forKey: indexPath)
        }
        
        // Try to find an existing data loader
        if let dataLoader = loadingOperations[indexPath] {
            // Has the data already been loaded?
            if let image = dataLoader.image {
                cell.updateAppearanceFor(image)
                loadingOperations.removeValue(forKey: indexPath)
            } else {
                // No data loaded yet, so add the completion closure to update the cell once the data arrives
                dataLoader.loadingCompleteHandler = updateCellClosure
            }
        } else {
            // Need to create a data loaded for this index path
            if let dataLoader = dataStore.loadImage(at: indexPath.row) {
                // Provide the completion closure, and kick off the loading operation
                dataLoader.loadingCompleteHandler = updateCellClosure
                loadingQueue.addOperation(dataLoader)
                loadingOperations[indexPath] = dataLoader
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // If there's a data loader for this index path we don't need it any more. Cancel and dispose
        if let dataLoader = loadingOperations[indexPath] {
            dataLoader.cancel()
            loadingOperations.removeValue(forKey: indexPath)
        }
    }
}

// MARK: - UITableViewDataSource
extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return cars.count
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath)
        -> UITableViewCell {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FancyCarsCell") as? FancyCarsTableViewCell else {
                fatalError("Sorry, could not load cell")
            }
            
            let car = cars[indexPath.row]
            
            let attributedString = NSMutableAttributedString(string: "")
            
            if let make = car.value(forKey: "make") as? String {
                let attriMake = NSAttributedString(string:make, attributes:
                    [NSAttributedString.Key.foregroundColor: UIColor.black,
                     NSAttributedString.Key.font: UIFont(name: "HelveticaNeue", size: 24.0)!])
                attributedString.append(attriMake)
            }
            
            if let model = car.value(forKey: "model") as? String {
                let modelAttribute = NSAttributedString(string:" " + model, attributes:
                    [NSAttributedString.Key.foregroundColor: UIColor.magenta,
                     NSAttributedString.Key.font: UIFont(name: "HelveticaNeue", size: 18.0)!])
                
                attributedString.append(modelAttribute)
            }
            
            if let name = car.value(forKey: "name") as? String {
                let nameAttribute = NSAttributedString(string:" " + name, attributes:
                    [NSAttributedString.Key.foregroundColor: UIColor.darkGray,
                     NSAttributedString.Key.font: UIFont(name: "Chalkduster", size: 18.0)!])
                
                attributedString.append(nameAttribute)
            }
            
            if let available = car.value(forKey: "available") as? String {
                
                if available == FancyCarsAPICarAvailabilityType.InDealership.rawValue {
                    cell.buyButton.isHidden = false
                    cell.buyButton.setContentCompressionResistancePriority(UILayoutPriority(rawValue: UILayoutPriority.Required), for: .horizontal)
                    
                    cell.detailsLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: UILayoutPriority.High), for: .horizontal)
                    
                    let yearAttribute = NSAttributedString(string:" " + available, attributes:
                        [NSAttributedString.Key.foregroundColor: UIColor.green,
                         NSAttributedString.Key.font: UIFont(name: "HelveticaNeue", size: 18.0)!])
                    
                    attributedString.append(yearAttribute)
                } else if available == FancyCarsAPICarAvailabilityType.OutofStock.rawValue {
                    cell.buyButton.isHidden = true
                    cell.buyButton.setContentCompressionResistancePriority(UILayoutPriority(rawValue: UILayoutPriority.High), for: .horizontal)
                    cell.detailsLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: UILayoutPriority.Required), for: .horizontal)
                    
                    let availableAttribute = NSAttributedString(string:" " + available, attributes:
                        [NSAttributedString.Key.foregroundColor: UIColor.orange,
                         NSAttributedString.Key.font: UIFont(name: "HelveticaNeue", size: 18.0)!])
                    
                    attributedString.append(availableAttribute)
                } else {
                    cell.buyButton.isHidden = true
                    cell.buyButton.setContentCompressionResistancePriority(UILayoutPriority(rawValue: UILayoutPriority.High), for: .horizontal)
                    cell.detailsLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: UILayoutPriority.Required), for: .horizontal)
                    
                    let availableAttribute = NSAttributedString(string:" " + available, attributes:
                        [NSAttributedString.Key.foregroundColor: UIColor.red,
                         NSAttributedString.Key.font: UIFont(name: "HelveticaNeue", size: 18.0)!])
                    
                    attributedString.append(availableAttribute)
                }
            }
            
            cell.detailsLabel?.attributedText = attributedString
            cell.updateAppearanceFor(UIImage(named: "car-placeholder"))
            return cell
    }
}

// MARK: - UITableViewDataSourcePrefetching
extension ViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        if let indexPath = indexPaths.first {
            if isLoadingCell(indexPath: indexPath) {
                self.fetchCars(sortBy: self.predicate, page: (cars.count + pageSize)/pageSize)
            }
        }
        
        for indexPath in indexPaths {
            if let _ = loadingOperations[indexPath] { return }
            if let dataLoader = dataStore.loadImage(at: indexPath.row) {
                loadingQueue.addOperation(dataLoader)
                loadingOperations[indexPath] = dataLoader
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cancelPrefetchingForRowsAt indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            if let dataLoader = loadingOperations[indexPath] {
                dataLoader.cancel()
                loadingOperations.removeValue(forKey: indexPath)
            }
        }
    }
}

extension UILayoutPriority {
    static var Low: Float { return 250.0 }
    static var High: Float { return 750.0 }
    static var Required: Float { return 1000.0 }
}

