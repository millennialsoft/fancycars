//
//  ImageModel.swift
//  FancyCars
//
//  Created by Akhlaq Rao on 4/8/19.
//  Copyright © 2019 MillennialSoft. All rights reserved.
//

import Foundation

struct ImageModel {
    public private(set) var url: URL?
    
    init(url: String?) {
        self.url = url?.toURL
    }
}

public extension String {
    var toURL: URL? {
        return URL(string: self)
    }
}
